// CRUD OPERATIONS
	// CRUD Operations is the heart of any backend application.
	// mastering the CRUD operations is essential for any developer.
	//This helps in building character and increasing exposure to logical statements that will help us manipulate
	//Mastering the CRUD operations of any languages makes us a valuavle developer and makes the work easier for us to deal with huge amounts of information

//[SECTION] CREATE (Insrting document):
	//Since MongoDB deals with object as it's structure for documents, we can easily create them by providing objects into our methods 
	// mongoDB shell also uses JS for it's syntax which makes it more convenient for us to understand it's code.

	// Inserting One Document
		/*
			Syntax:
				db.collectionName.insertOne({object/document});

		*/

	db.users.inserOne({
		firstName : "Jane",
		lastName : "Doe",
		age:21,
		contact:{
			phone:"123456789",
			email:"janedoe@gmail.com"
		},
		courses:["CSS", "JavaScript", "Python"],
		department:"none"

	});



	// INSERT MANY

	/*
		Syntax:
			db.users.insertMany([{objectA}, {objectB}])

	*/

	db.users.insertMany([
	{
		firstName : "Stephen",
		lastName : "Hawking",
		age:76,
		contact:{
			phone:"87654321",
			email:"stephenhawking@gmail.com"
		},
		courses:["Python", "React", "PHP"],
		department:"none"
	},	
	{
		firstName : "Niel",
		lastName : "Armstrong",
		age:82,
		contact:{
			phone:"87654321",
			email:"nielarmstrong@gmail.com"
		},
		courses:["React", "Laravel", "Sass"],
		department:"none"
	}]);


	db.users.inserOne({
		firstName : "Jane",
		lastName : "Doe",
		age:21,
		contact:{
			phone:"123456789",
			email:"janedoe@gmail.com"
		},
		courses:["CSS", "JavaScript", "Python"],

	});

	db.users.insertOne({
		firstName : "Jane",
		lastName : "Doe",
		age:21,
		contact:{
			phone:"123456789",
			email:"janedoe@gmail.com"
		},
		courses:["CSS", "JavaScript", "Python"],
		gender:"female"

	});


	// FIND (READ)
		/*
			Syntax:
				//it will show us all the douments in our collection	
				db.collectionName.find();
				
				//it will show us all the documentthat has the given fildset
				db.collectionName({field:value})

		*/ 

	db.users.find({age:76});

		//fingding documents using multiple fieldset
		
		/*
			Syntax:
				db.colletionName.find({fieldA:valueA, fieldB:valueB});
		*/

	db.users.find({lastName:"Armstrong", age:82});




	//UPDATING DOCUMENTS

	 //add document
	 	db.users.insertOne({
	 		firstName : "Test",
			lastName : "Test",
			age:0,
			contact:{
				phone:"000000000",
				email:"test@gmail.com"
			},
			courses:[],
			department:"none"
	 	});

	 // updateOne
	 	/*
			Syntax:
			db.collectionName.updateOne({criteria},{$set: {field:value}})

	 	*/

	 	db.users.updateOne({firstName"Jane"}, {$set:{lastName:"Hawking";}})

	//updating multiple documents
	/*
		db.collectionName.updateMany({criteria}{$set:{field:value}})
	*/ 

	 	db.users.updateMany({firstName:"Jane"},{$set:{
	 		lastName:"Wick",
	 		age:25
	 	}})

	 	db.users.updateMany({department:"none"},{$set:{
	 		department:"HR"
	 	}})


	// replacing the old document
	 	/*
			Syntax:
				db.users.replaceOne({citeria}, {document/objectToReplace})
	 	*/

	 	db.users.replaceOne({firstName:"Test"}, 
	 		{
	 			firstName: "Chris", 
	 			lastName:"Mortel"
	 		});

	// DELETING DOCUMENTS

	 	// Deleting single document
	 	/*
			db.collectionName.deleteOne({criteria});
	 	*/

	 	db.users.deleteOne({firstName:"Jane"});


	 	// deleting many 
	 	// DO NOT FORGET TO ADD CRITERIA!!!!! ALL THE DOCUMENTS WILL BE DELETED!!!!! 
	 	// ALWAYS PUT CRITERIA.
	 	/*
	 	syntax:
			db.collectionName.deleteMany({criteria});
	 	*/

	 	db.users.deleteMany({firstName:"Jane"});


//[SECTION] ADVANCE QUERIES
	//embedded = object
	//nested field = array 

	//Query on an embedded document (USING DOT (.) NOTATION)
		db.users.find({contact.phone:"87654321"}); 

	// Query on an array with exact elements
		db.users.find({courses:["React", "Laravel", "Sass"]});

	//Querying an array without regards to oirder and elemnents

	db.users.find({courses:{$all:["React"]}}); 